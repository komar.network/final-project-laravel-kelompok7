<?php

use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//   return view('welcome');
// });

Auth::routes();

Route::middleware(['auth'])->group(function () {
  Route::get('/',  [HomeController::class, 'index'])->name('dashboard');
  Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
  Route::resource('products', ProductController::class);
  Route::get('setting', [SettingController::class, 'index'])->name('setting.index');
  Route::post('setting', [SettingController::class, 'store'])->name('setting.store');
  Route::resource('customers', CustomerController::class);
  Route::resource('orders', OrderController::class);

  Route::get('/cart', [CartController::class, 'index'])->name('cart.index');
  // Route::get('/cart/{id}', [CartController::class, 'store'])->name('cart.store');
  Route::post('/cart', [CartController::class, 'store'])->name('cart.store');
  Route::get('/cart/{id}', [CartController::class, 'addToCart'])->name('add_to_cart');
  Route::patch('/cart/update', [CartController::class, 'updateCart'])->name('update_cart');
  Route::post('/cart/change-qty', [CartController::class, 'changeQty'])->name('changeQty');
  Route::delete('/cart/delete', [CartController::class, 'delete'])->name('cart.delete');
  Route::delete('/cart/empty', [CartController::class, 'empty']);
});
