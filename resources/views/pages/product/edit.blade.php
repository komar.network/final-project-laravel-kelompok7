@extends('adminlte.master')

@section('title', 'Edit Product')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Edit product</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')

      <div class="form-group">
        <label for="name">Nama Product</label>
        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
          placeholder="Nama Product" value="{{ old('name', $product->name) }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="description">Deskripsi Product</label>
        <input type="text" name="description" class="form-control @error('description') is-invalid @enderror"
          id="description" placeholder="Deskripsi Product" value="{{ old('description', $product->description) }}">
        @error('description')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="image">Gambar Product</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input" @error('image') is-invalid @enderror name="image" id="image"
            onchange="previewImage()">
          <label class="custom-file-label" for="image">Choose file</label>

          <!-- menampilkan gambar lama -->
          <input type="hidden" name="oldImage" value="{{ $product->image }}">
          @if ($product->image)
          <img src="{{ asset('storage/'. $product->image) }}" class="image-preview img-fluid mt-2 col-sm-3 d-block">
          @else
          <img class="image-preview img-fluid col-sm-3">
          @endif
          <br>
        </div>
        @error('image')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="barcode">Barcode</label>
        <input type="text" name="barcode" class="form-control @error('barcode') is-invalid @enderror" id="barcode"
          placeholder="Barcode" value="{{ old('barcode', $product->barcode) }}">
        @error('barcode')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="price">Harga</label>
        <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" id="price"
          placeholder="Harga" value="{{ old('price', $product->price) }}">
        @error('price')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="address">Jumlah Product</label>
        <input type="text" name="quantity" class="form-control @error('quantity') is-invalid @enderror" id="quantity"
          placeholder="Jumlah Product" value="{{ old('quantity', $product->quantity) }}">
        @error('quantity')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="status">Status</label>
        <select name="status" class="form-control @error('status') is-invalid @enderror" id="status">
          <option value="1" {{ old('status', $product->status) === 1 ? 'selected' : ''}}>Active</option>
          <option value="0" {{ old('status', $product->status) === 0 ? 'selected' : ''}}>Inactive</option>
        </select>
        @error('status')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Update</button>
    </form>

  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
  $(document).ready(function () {
            bsCustomFileInput.init();
        });

  function previewImage() {
    const image = document.querySelector('#image');
    const imagePreview = document.querySelector('.image-preview');

    imagePreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
        imagePreview.src = oFREvent.target.result;
    }
  }
</script>
@endpush