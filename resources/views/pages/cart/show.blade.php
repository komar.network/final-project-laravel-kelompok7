@extends('adminlte.master')

@section('title', 'Halaman Produk')

@section('content')
<div class="col-12">
  <div class="card">
  <div class="card-header">
    <h3 class="card-title">Tampilkan Produk</h3>
    <div class="card-tools">
      <a href="/products" class="btn btn-tool btn-sm btn-success">
        <i class="fas fa-arrow-left"></i>
      </a>
    </div>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <strong><i class=""></i> Nama Produk</strong>
    <p class="text-muted">
      {{ $product->name }}
    </p>
    <hr>
    <strong><i class=""></i> Deskripsi Produk</strong>
    <p class="text-muted">
      {{ $product->description }}
    </p>
    <hr>
    <strong><i class=""></i> Foto Produk</strong>
    <p class="text-muted">
      <img src="{{ asset('.../image/' . $product->image) }}" alt="">
    </p>
    <hr>
    <strong><i class=""></i> Barcode</strong>
    <p class="text-muted">
      {{ $product->barcode }}
    </p>
    <hr>
    <strong><i class=""></i> Harga Produk</strong>
    <p class="text-muted">
      {{ $product->price }}
    </p>
    <hr>
    <strong><i class=""></i> Jumlah Produk</strong>
    <p class="text-muted">
      {{ $product->quantity }}
    </p>
    <hr>
  </div>
  </div>
</div>
@endsection