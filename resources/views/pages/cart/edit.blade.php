@extends('adminlte.master')

@section('title', 'Halaman Produk')

@section('content')
<div class="col-12">
  <div class="card">
  <div class="card-header">
    <h3 class="card-title">Ubah Produk</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="name">Nama Produk</label>
        <input type="text" class="form-control" name="name" value="{{$product->name}}" id="name" placeholder="Masukkan Nama Produk">
        @error('name')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="description">Deskripsi Produk</label>
        <input type="text" class="form-control" name="description" value="{{$product->description}}" id="description" placeholder="Masukkan Deskripsi Produk">
        @error('description')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="image">Gambar Produk</label>
        <input type="file" class="form-control" name="image" value="{{$product->image}}" id="image" placeholder="Masukkan File Image Produk">
        @error('image')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="barcode">Barcode</label>
        <input type="text" class="form-control" name="barcode" value="{{$product->barcode}}" id="barcode" placeholder="Masukkan Barcode Produk">
        @error('barcode')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="price">Harga Produk</label>
        <input type="number" class="form-control" name="price" value="{{$product->price}}" id="price" placeholder="Masukkan Harga Produk">
        @error('price')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="quantity">Jumlah Produk</label>
        <input type="number" class="form-control" name="quantity" value="{{$product->quantity}}" id="quantity" placeholder="Masukkan Jumlah Produk">
        @error('quantity')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Update</button>
    </form>

  </div>
  </div>
</div>
@endsection