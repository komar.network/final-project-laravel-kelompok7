@extends('adminlte.master')

@section('title', 'Halaman Order')

@section('content')
<div class="row">
  <div class="col-md-6 col-lg-4">
    <div class="row mb-2">
      <div class="col">
        {{-- <form action="">
          <input type="text" class="form-control" placeholder="Scan Barcode..." value={{ $products->barcode }}/>
        </form> --}}
      </div>
      <div class="col">
        <select name="" id="" class="form-control">
          <option value="">Walking Customer</option>
        </select>
      </div>
    </div>
    <form action="{{ route('cart.store') }}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="card">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Quantity</th>
              <th class="text-right">Price</th>
            </tr>
          </thead>
          <tbody>
            @php $total = 0 @endphp
            @if (session('cart'))
            @foreach (session('cart') as $id => $details)
            @php $total += $details['price'] * $details['quantity'] @endphp
            <tr data-id="{{ $id }}">
              <td>{{ $details['name'] }}</td>
              <td>
                <input type="number" name="quantity" value="{{ $details['quantity'] }}"
                  class="form-control quantity cart_update" min="1">
                <button class=" btn btn-danger btn-sm cart_remove">
                  <i class="fas fa-trash"></i>
                </button>
              </td>
              <td><input type="hidden" name="price" value="{{ $total }}">{{
                $details['price'] * $details['quantity'] }}
              </td>
            </tr>
            @endforeach
            @endif

          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col">Total:</div>
        <div class="col text-right">{{ $total }}</div>
      </div>
      <div class="row">

        <div class="col">
          <button type="button" class="btn btn-danger btn-block">Cancel</button>
        </div>
        <div class="col">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-6 col-lg-8">
    <div class="mb-2">
      <input type="text" class="form-control" placeholder="Search Product..." />
    </div>

    <div class="row">
      @forelse ($products as $item)
      <div class="col">
        <div class="card">
          <div class="card-body">
            <img src="{{ asset('storage/'. $item->image) }}" class="img-fluid" width="200px" height="auto" alt="" />
            <h5>{{ $item->name }} ({{ $item->quantity }})</h5>
            <a href="{{ route('add_to_cart', $item->id) }}" class="btn btn-primary">Order</a>
          </div>
        </div>
      </div>
      @empty
      @endforelse
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src=" {{ asset('plugins/datatables/jquery.dataTables.min.js') }}">
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}">
</script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}">
</script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}">
</script>

<script>
  $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });

    $(".cart_update").change(function (e) {
        e.preventDefault();

        var ele = $(this);

        $.ajax({
            url: '{{ route('update_cart') }}',
            method: "patch",
            data: {
                _token: '{{ csrf_token() }}',
                id: ele.parents("tr").attr("data-id"),
                quantity: ele.parents("tr").find(".quantity").val()
            },
            success: function (response) {
               window.location.reload();
            }
        });
    });

    $(".cart_remove").click(function (e) {
        e.preventDefault();

        var ele = $(this);

        if(confirm("Do you really want to remove?")) {
            $.ajax({
                url: '{{ route('cart.delete') }}',
                method: "DELETE",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: ele.parents("tr").attr("data-id")
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });
</script>

@endpush