@extends('adminlte.master')

@section('title', 'Customer')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Create Customer</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="{{ route('customers.update', $customer->id) }}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')

      <div class="form-group">
        <label for="first_name">First Name</label>
        <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror"
          id="first_name" placeholder="First Name" value="{{ old('first_name', $customer->first_name) }}">
        @error('first_name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="last_name">Last Name</label>
        <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" id="last_name"
          placeholder="Last Name" value="{{ old('last_name', $customer->last_name) }}">
        @error('last_name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
          placeholder="Email" value="{{ old('email', $customer->email) }}">
        @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone"
          placeholder="Phone" value="{{ old('phone', $customer->phone) }}">
        @error('phone')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="address">Address</label>
        <input type="text" name="address" class="form-control @error('address') is-invalid @enderror" id="address"
          placeholder="Address" value="{{ old('address', $customer->address) }}">
        @error('address')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="avatar">Avatar</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input" @error('avatar') is-invalid @enderror name="avatar" id="image"
            onchange="previewImage()">
          <label class="custom-file-label" for="avatar">Choose file</label>

          <!-- menampilkan gambar lama -->
          <input type="hidden" name="oldImage" value="{{ $customer->avatar }}">
          @if ($customer->avatar)
          <img src="{{ asset('storage/'. $customer->avatar) }}" class="image-preview img-fluid mt-2 col-sm-3 d-block">
          @else
          <img class="image-preview img-fluid col-sm-3">
          @endif
          <br>
        </div>
        @error('avatar')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Update</button>
    </form>

  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
  $(document).ready(function () {
            bsCustomFileInput.init();
        });

  function previewImage() {
    const image = document.querySelector('#image');
    const imagePreview = document.querySelector('.image-preview');

    imagePreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
        imagePreview.src = oFREvent.target.result;
    }
  }
</script>
@endpush