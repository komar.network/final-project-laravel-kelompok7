@extends('adminlte.master')

@section('title', 'Customer')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Create Customer</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="{{ route('customers.store') }}" method="POST" enctype="multipart/form-data">
      @csrf

      <div class="form-group">
        <label for="first_name">First Name</label>
        <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror"
          id="first_name" placeholder="First Name" value="{{ old('first_name') }}">
        @error('first_name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="last_name">Last Name</label>
        <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" id="last_name"
          placeholder="First Name" value="{{ old('last_name') }}">
        @error('last_name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
          placeholder="Email" value="{{ old('email') }}">
        @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone"
          placeholder="Phone" value="{{ old('phone') }}">
        @error('phone')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="address">Address</label>
        <textarea class="form-control @error('address') is-invalid @enderror" name="address" id="address" rows="3"
          placeholder="Address" value="{{ old('address') }}"></textarea>
        {{-- <input type="text" name="address" class="form-control @error('address') is-invalid @enderror" id="address"
          placeholder="Address" value="{{ old('address') }}"> --}}
        @error('address')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="avatar">Avatar</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="avatar" id="avatar">
          <label class="custom-file-label" for="avatar">Choose file</label>
        </div>
        @error('avatar')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
  $(document).ready(function () {
            bsCustomFileInput.init();
        });
</script>
@endpush