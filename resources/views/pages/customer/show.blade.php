@extends('adminlte.master')
@section('content')

<div class="container justify-content-center">
  <div class="col-md-12">

    <!-- Profile Image -->
    <div class="card card-primary card-outline">
      <div class="card-body box-profile">
        <div class="text-center">
          <img class="profile-user-img img-fluid img-circle" src="{{ asset('storage/'. $customer->avatar) }}"
            alt="User profile picture">
        </div>
        <h3 class="profile-username text-center">{{ auth()->user()->getFullname() }}</h3>

        <p class="text-muted text-center">FullStack Developer</p>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

    <!-- About Me Box -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tentang Saya</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <strong><i class="fas fa-phone mr-1"></i>Phone</strong>
        <p class="text-muted">{{ $customer->phone}}</p>
        <hr>
        <strong><i class="fas fa-envelope mr-1"></i>Email</strong>
        <p class="text-muted">{{ $customer->email}}</p>
        <hr>
        <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>
        <p class="text-muted">{{ $customer->alamat}}</p>
        <hr>
        <a href="{{ route('customers.index') }}" class="btn btn-primary btn-block"><b>Kembali ke Index</b></a>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
@endsection