@extends('adminlte.master')

@section('title', 'cast')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Cast Create</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="{{ route('cast_store') }}" method="POST" enctype="multipart/form-data">
      @csrf

      <div class="form-group">
        <label for="nama">Name</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Pemeran">
        @error('nama')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="form-group">
        <label for="bio">Biodata</label>
        <textarea type="text" class="form-control" name="bio" id="bio" placeholder="Tulis Biodata Pemeran"></textarea>
        @error('name')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

  </div>
</div>
@endsection