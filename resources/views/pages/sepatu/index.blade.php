@extends('adminlte.master')

@section('title', 'Halaman Sepatu')

@section('content')
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Datatables Sepatu</h3>

      <div class="card-tools">
        <a href="{{ route('sepatu.create') }}" class="btn btn-tool btn-sm">
          <i class="fas fa-plus"></i>
        </a>
      </div>

    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
        </thead>
        {{-- <tbody>
          @foreach ($cast as $item)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td> {{ $item->bio }}</td>
            <td>
              <a href="{{ route('cast_edit', $item->id) }}" class="btn btn-secondary">
                <i class="fas fa-edit"></i>
              </a>
              <a href="{{ route('cast_show', $item->id) }}" class="btn btn-secondary">
                <i class="fas fa-eye"></i>
              </a>

              <a class="btn btn-danger" onclick="
                                var result = confirm('Are you sure you want to delete this record?');

                                if(result){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{$item->id}}').submit();
                                }">
                <i class="fas fa-trash"></i>
              </a>

              <form action="{{ route('cast_destroy', $item->id) }}" method="post" id="delete-form-{{$item->id}}">
                @method('DELETE')
                @csrf
              </form>
            </td>

          </tr>
          @endforeach
        </tbody> --}}
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
@endsection

@push('scripts')
<script src=" {{ asset('plugins/datatables/jquery.dataTables.min.js') }}">
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}">
</script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}">
</script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}">
</script>

<script>
  $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>

@endpush