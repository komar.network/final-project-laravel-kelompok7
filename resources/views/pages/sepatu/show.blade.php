@extends('adminlte.master')

@section('title', 'cast')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Cast Show</h3>
    <div class="card-tools">
      <a href="{{ route('cast') }}" class="btn btn-tool btn-sm btn-success">
        <i class="fas fa-arrow-left"></i>
      </a>
    </div>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <strong><i class="fas fa-book mr-1"></i> Nama</strong>
    <p class="text-muted">
      {{ $cast->nama }}
    </p>
    <hr>
    <strong><i class="fas fa-birthday-cake mr-1"></i> Umur</strong>
    <p class="text-muted">{{ $cast->umur }}</p>
    <hr>
    <strong><i class="fas fa-pencil-alt mr-1"></i> Biodata</strong>
    <p class="text-muted">
      {{ $cast->bio }}
    </p>
    <hr>

  </div>
</div>
@endsection