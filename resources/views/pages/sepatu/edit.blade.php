@extends('adminlte.master')

@section('title', 'cast')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Cast Edit</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">

    <h2>Edit Cast {{$cast->nama}}</h2>
    <form action="{{ route('cast_update', $cast->id) }}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama"
          placeholder="Masukkan nama">
        @error('nama')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">umur</label>
        <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="umur"
          placeholder="Masukkan umur">
        @error('umur')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio"
          placeholder="Masukkan bio">{{ old('bio', $cast->bio) }}</textarea>
        @error('bio')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Update</button>
    </form>

  </div>
</div>
@endsection