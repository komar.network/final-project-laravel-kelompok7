@extends('adminlte.master')

@section('title', 'Update Setting')

@section('content')
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Setting</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{ route('setting.store') }}" method="post">
        @csrf

        <div class="form-group">
          <label for="app_name">App name</label>
          <input type="text" name="app_name" class="form-control @error('app_name') is-invalid @enderror" id="app_name"
            placeholder="App name" value="{{ old('app_name', config('settings.app_name')) }}">
          @error('app_name')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="form-group">
          <label for="app_description">App description</label>
          <textarea name="app_description" class="form-control @error('app_description') is-invalid @enderror"
            id="app_description"
            placeholder="App description">{{ old('app_description', config('settings.app_description')) }}</textarea>
          @error('app_description')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="form-group">
          <label for="currency_symbol">Currency symbol</label>
          <input type="text" name="currency_symbol" class="form-control @error('currency_symbol') is-invalid @enderror"
            id="currency_symbol" placeholder="Currency symbol"
            value="{{ old('currency_symbol', config('settings.currency_symbol')) }}">
          @error('currency_symbol')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label for="warning_quantity">Warning quantity</label>
          <input type="text" name="warning_quantity"
            class="form-control @error('warning_quantity') is-invalid @enderror" id="warning_quantity"
            placeholder="Warning quantity" value="{{ old('warning_quantity', config('settings.warning_quantity')) }}">
          @error('warning_quantity')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <button type="submit" class="btn btn-primary">Change Setting</button>
      </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src=" {{ asset('plugins/datatables/jquery.dataTables.min.js') }}">
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}">
</script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}">
</script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}">
</script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}">
</script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}">
</script>

<script>
  $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>

@endpush