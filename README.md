# Final Project Laravel Kelompok 7 Bath 48 Sanbercode

## Anggita Kelompok
- [ ] Ichwanutakwa Bagaskara ( https://t.me/Bagaskara334 )
- [ ] komarudin (@End_Code_01011 )
- [ ] Luluk Lathifah Zain (@luluklzain )

## Desain ERD
![IMAGE_DESCRIPTION](ERD.png)

## Tema Project
- [ ] Point Of Sale


## Setup Project
Repositori ini dibangun dengan Laravel versi 9 ke atas. Setelah melakukan clone dari repositori ini, lakukanlah langkah-langkah di bawah ini untuk menjalankan project. 

* git clone => with ssh
```bash
$ git clone git@gitlab.com:komar.network/final-project-laravel-kelompok7.git
```
* git clone => with https
```bash
$ git clone https://gitlab.com/komar.network/final-project-laravel-kelompok7.git
```

* masuk ke direktori final-project-laravel-kelompok7
```bash
$ cd final-project-laravel-kelompok7
```

* jalankan perintah composer install untuk mendownload direktori vendor
```bash
$ composer install
```

* buat file .env lalu isi file tersebut dengan seluruh isi dari file .env.example

* jalankan perintah php artisan key generate
```bash
$ php artisan key:generate
```

* jalankan perintah npm install untuk mendownload package
```bash
$ npm install && npm run dev
```

* Tambahan: Untuk pengerjaan di laptop/PC masing-masing, sesuaikan nama database, username dan password nya di file .env dengan database kalian. 

Setelah itu kalian sudah bisa lanjut mengerjakan soal berikutnya. jangan lupa untuk menjalankan server laravel
```bash
$ php artisan serve
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/komar.network/final-project-laravel-klinik/-/settings/integrations)

## Test and Deploy

Link Demo

- [ ] [VPS](#)

***

## Project status
Start from 23 Agustus 2023
