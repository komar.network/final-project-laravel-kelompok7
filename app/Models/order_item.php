<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class order_item extends Model
{
  use HasFactory;

  protected $table = 'order_items';

  protected $fillable = [
    'price',
    'quantity',
    'product_id',
    'order_id'
  ];

  public function product()
  {
    return $this->belongsTo(Product::class);
  }
}
