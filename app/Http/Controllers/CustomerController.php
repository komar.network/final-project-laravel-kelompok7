<?php

namespace App\Http\Controllers;

use App\Models\customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CustomerStoreRequest;

class CustomerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
    if (request()->wantsJson()) {
      return response(
        customer::all()
      );
    }
    $customers = customer::latest()->paginate(10);
    return view('pages.customer.index')->with('customers', $customers);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
    return view('pages.customer.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $validatedData = $request->validate([
      'first_name'  => 'required|max:20|unique:customers',
      'last_name'  => 'required|max:20',
      'email'   => 'required',
      'phone'   => 'nullable',
      'address'   => 'nullable',
      'avatar'   => 'required|mimes:jpeg,png,jpg|max:100'
    ]);

    if ($request->file()) {
      $fileName = $request->first_name . '-' . time() . '.' . $request->avatar->extension();
      $filePath = $request->file('avatar')->storeAs('customers', $fileName, 'public');

      $validatedData['avatar'] = $filePath;
    }

    $validatedData['user_id'] = $request->user()->id;

    customer::create($validatedData);

    return redirect()->route('pages.customer.index')->with('success', 'Customer berhasil dibuat');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
    $customer = customer::FindOrFail($id);
    return view('pages.customer.show', ['customer' => $customer]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
    $customer = customer::FindOrFail($id);
    return view('pages.customer.edit', ['customer' => $customer]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
    $validatedData = $request->validate([
      'first_name'  => 'required|max:20',
      'last_name'  => 'required|max:20',
      'email'   => 'required',
      'phone'   => 'nullable',
      'address'   => 'nullable',
      'avatar'   => 'nullable|mimes:jpeg,png,jpg|max:100'
    ]);

    // jika type file dengan value 'nama_colum', maka
    if ($request->file('avatar')) {
      // jika gambar lama ada, maka kita akan Delete
      if ($request->oldImage) {
        Storage::delete($request->oldImage);
      }
      # jika ada gambar maka kita Upload
      $fileName = $request->first_name . '-' . time() . '.' . $request->avatar->extension();
      $filePath = $request->file('avatar')->storeAs('customers', $fileName, 'public');

      $validatedData['avatar'] = $filePath;
    }

    $validatedData['user_id'] = $request->user()->id;

    customer::findOrFail($id)->update($validatedData);

    return redirect()->route('pages.customer.index')->with('success', 'Success, data customer berhasil di edit.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
    $customer = customer::FindOrFail($id);

    $filePath = $customer->avatar;
    Storage::disk('public')->delete($filePath);

    $customer->delete();
    return response()->json(['success' => $customer->first_name . ' Berhasil Dihapus!']);
  }
}
