<?php

namespace App\Http\Controllers;

use App\Models\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if (request()->wantsJson()) {
      return response(
        product::all()
      );
    }
    $products = product::latest()->paginate(10);
    return view('pages.product.index')->with('products', $products);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('pages.product.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'name'  => 'required|max:20|unique:products',
      'description'  => 'required|max:20',
      'image'   => 'nullable|mimes:jpeg,png,jpg|max:100',
      'barcode'   => 'nullable|string|max:50|unique:products',
      'price'   => 'required|regex:/^\d+(\.\d{1,2})?$/',
      'quantity'   => 'required|integer',
      'status' => 'required|boolean'
    ]);

    $filePath = '';

    if ($request->file()) {
      $fileName = $request->name . '-' . time() . '.' . $request->image->extension();
      $filePath = $request->file('image')->storeAs('products', $fileName, 'public');

      $validatedData['image'] = $filePath;
    }

    $product = product::create($validatedData);

    if (!$product) {
      return redirect()->back()->with('error', 'Maaf, kamu belum gagal menambahkan product.');
    }
    return redirect()->route('products.index')->with('success', 'Berhasil, kamu berhasil menambahkan product.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $products = product::find($id);

    return view('pages.product.show', ['product' => $products]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $products = product::find($id);

    return view('pages.product.edit', ['product' => $products]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validatedData = $request->validate([
      'name'  => 'required|max:20',
      'description'  => 'required|max:20',
      'image'   => 'nullable|mimes:jpeg,png,jpg|max:100',
      'barcode'   => 'nullable|string|max:50|unique:products',
      'price'   => 'required|regex:/^\d+(\.\d{1,2})?$/',
      'quantity'   => 'required|integer',
      'status' => 'required|boolean'
    ]);

    // jika type file dengan value 'nama_colum', maka
    if ($request->file('image')) {
      // jika gambar lama ada, maka kita akan Delete
      if ($request->oldImage) {
        Storage::delete($request->oldImage);
      }
      # jika ada gambar maka kita Upload
      $fileName = $request->name . '-' . time() . '.' . $request->image->extension();
      $filePath = $request->file('image')->storeAs('products', $fileName, 'public');

      $validatedData['image'] = $filePath;
    }

    product::findOrFail($id)->update($validatedData);

    return redirect()->route('products.index')->with('success', 'Success, data product berhasil di edit.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $product = product::FindOrFail($id);

    $filePath = $product->image;
    Storage::disk('public')->delete($filePath);

    $product->delete();
    return response()->json(['success' => 'Berhasil Dihapus!']);
  }
}
