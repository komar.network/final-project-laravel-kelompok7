<?php

namespace App\Http\Controllers;

use App\Models\setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
    return view('pages.setting.edit');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
    $data = $request->except('_token');
    foreach ($data as $key => $value) {
      $setting = setting::firstOrCreate(['key' => $key]);
      $setting->value = $value;
      $setting->save();
    }

    return redirect()->route('setting.index');
  }
}
