<?php

namespace App\Http\Controllers;

use App\Models\order;
use App\Models\order_item;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    if ($request->wantsJson()) {
      // return response(
      //   $request->user()->cart()->get()
      // );
      return response(
        product::all()
      );
    }

    $products = product::all();
    return view('pages.cart.index')->with('products', $products);
  }

  public function addToCart($id)
  {
    //
    $product = Product::findOrFail($id);

    $cart = session()->get('cart', []);

    if (isset($cart[$id])) {
      $cart[$id]['quantity']++;
    } else {
      $cart[$id] = [
        'name' => $product->name,
        'image' => $product->image,
        'price' => $product->price,
        'quantity' => 1
      ];
    }

    session()->put('cart', $cart);
    return redirect()->back()->with('success', 'berhasil ditambahkan');
  }

  public function updateCart(Request $request)
  {
    if ($request->id && $request->quantity) {
      $cart = session()->get('cart');
      $cart[$request->id]["quantity"] = $request->quantity;
      session()->put('cart', $cart);
      session()->flash('success', 'Cart successfully updated!');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    echo ("<h3>Maaf kang belum selesai, seadanya aja : dikarenakan tim kurang maksimal</h3>");
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  public function changeQty(Request $request)
  {
    $request->validate([
      'product_id' => 'required|exists:products,id',
      'quantity' => 'required|integer|min:1',
    ]);

    $product = Product::find($request->product_id);
    $cart = $request->user()->cart()->where('id', $request->product_id)->first();

    if ($cart) {
      // check product quantity
      if ($product->quantity < $request->quantity) {
        return response([
          'message' => 'Product available only: ' . $product->quantity,
        ], 400);
      }
      $cart->pivot->quantity = $request->quantity;
      $cart->pivot->save();
    }

    return response([
      'success' => true
    ]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function delete(Request $request)
  {
    // $request->validate([
    //   'product_id' => 'required|integer|exists:products,id'
    // ]);
    // $request->user()->cart()->detach($request->product_id);

    // return response('', 204);

    if ($request->id) {
      $cart = session()->get('cart');
      if (isset($cart[$request->id])) {
        unset($cart[$request->id]);
        session()->put('cart', $cart);
      }
      session()->flash('success', 'Product berhasil dihapus!');
    }
  }

  public function empty(Request $request)
  {
    $request->user()->cart()->detach();

    return response('', 204);
  }
}
